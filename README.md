# IH homework 2022 02

## New Input System demo

## Author
Buchin Yury

## Special thanks
Daria Kolgatina

BIT.GAMES

## ThirdPartyNotice
Asset is governed by the Asset Store EULA; however, the following components are governed by the licenses indicated below:
A. Roboto Black

## HomeWork
* (Done) Сделать базовое управление машинкой по WASD
* (Done) Добавить управление стрелочками
* (Done) Добавить управление с помощью джостика
* (Done) Сделать ускорение карта по нажатию на правую клавишу мыши